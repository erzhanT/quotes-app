import React from 'react';
import './App.css';
import {Route, Switch} from 'react-router-dom';
import Submit from "./containers/Submit/Submit";
import Quotes from "./containers/Quotes/Quotes";
import Header from "./components/Header/Header";
import Edit from "./containers/Edit/Edit";

const App = () => {


    return (
        <>
            <Header/>
            <Switch>
                <Route path={'/quotes'} exact component={Quotes}/>
                <Route path={'/add-quote'} exact component={Submit}/>
                <Route path={'/quotes/:id/edit'} exact component={Edit}/>

            </Switch>
        </>
    )
};

export default App;
