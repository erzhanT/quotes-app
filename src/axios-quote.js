import axios from "axios";

const axiosQuote = axios.create({
    baseURL: 'https://qoutes-erzhan-default-rtdb.firebaseio.com/'
})

export default axiosQuote;