import React from 'react';
import './Header.css';
import {NavLink} from 'react-router-dom';

const Header = () => {
    return (
        <div className={'nav'}>
            <h2 className="name">Quotes</h2>
            <div className="main-item">
                <div className={'item'}>
                    <NavLink className="link" to={'/quotes/'}>Quotes</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/add-quote'}>Submit new quote</NavLink>
                </div>
            </div>

        </div>
    );
};

export default Header;