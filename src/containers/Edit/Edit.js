import React, {useEffect, useState} from 'react';
import axiosQuote from "../../axios-quote";
import {CATEGORY} from "../../constans";
import './Edit.css';

const Edit = props => {

    const [edit, setEdit] = useState({});
    const id = props.match.params.id

    const onChangeEdit = event => {

        const {name, value} = event.target;
        setEdit(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    useEffect(() => {
        axiosQuote.get(`/quotes/${id}.json`).then(response => {
            setEdit(response.data);
        });
    }, [id]);


    const editPost = () => {
        const data = {
            category: edit.category,
            quote: edit.quote,
            author: edit.author
        }
        axiosQuote.put(`/quotes/${id}.json`, data).then(() => {
            props.history.push('/quotes');
        })
    }
    console.log(props.match.params.id)
    return (
        <div className="form">
            <div className="Submit">


            <h2>Edit a quote</h2>
            <select name="category" onChange={onChangeEdit}>
                <option />
                {CATEGORY.map(cat => (
                    <option key={cat.id} name='star-wars' value={cat.id}>{cat.title}</option>
                ))}
            </select>
            <input
                type="text"
                name="author"
                value={edit.author}
                onChange={(e) => onChangeEdit(e)}
            />
            <textarea
                cols="30"
                rows="10"
                name="quote"
                value={edit.quote}
                onChange={(e) => onChangeEdit(e)}
            />
            <button className="save" onClick={editPost}>Save</button>
            </div>
        </div>
    );
};

export default Edit;