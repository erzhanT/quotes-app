import React, {useState, useEffect} from 'react';
import axiosQuote from "../../axios-quote";
import {NavLink} from "react-router-dom";
import {CATEGORY} from "../../constans";

const Quotes = props => {

    const [quote, setQuote] = useState('');
    const id = props.match.params.id;


    console.log(props.match)

    useEffect(() => {
        axiosQuote.get('/quotes.json').then(response => {
            setQuote(response.data)
        }, e => {
            console.log(e);
        });
    }, []);

    const removeQuote = () => {
        return ( axiosQuote.delete(`/quotes/${id}.json`).then(() => {
            props.history.push('/quotes')
        })
    );
    };


    console.log(id);
    const checkQuote = () => {
        if (!quote) {
            return <h1>None Quotes</h1>
        } else {
            return (
                <div className="Quotes">
                    {Object.keys(quote).map(key => (
                        <div className="Quote" key={key}>
                            <h5>Author: {quote[key].author}</h5>
                            <p>{quote[key].quote}</p>
                            <button className="save" onClick={() => removeQuote(key)}>Delete</button>
                            <NavLink to={`/quotes/${key}/edit`}>Edit</NavLink>
                            {console.log(key)}
                        </div>

                    ))}
                </div>
            )
        }
    }




    return (
        <div >
            <div className="list" >
                <ul>
                    {CATEGORY.map(cat => (
                        <li key={cat.id}>
                            <NavLink to={`quotes/?orderBy="category"&equalTo="${cat.id}"`}>{cat.title}</NavLink>
                        </li>
                    ))}
                </ul>
            </div>
            {checkQuote()}
        </div>
    );
};

export default Quotes;