import React, {useState} from 'react';
import axiosQuote from "../../axios-quote";
import {CATEGORY} from "../../constans";

const Submit = props => {
    const [submit, setSubmit] = useState({
        quote: '',
        author: ''
    });



    const quoteDataChanged = event => {
        const {name, value} = event.target;
        setSubmit(prevState => ({
            ...prevState,
            [name]: value
        }))
    };


    const quoteHandler = async event => {
        event.preventDefault();

        const obj = {
            category: submit.category,
            quote: submit.quote,
            author: submit.author
        }
        try {
            await axiosQuote.post('/quotes.json', obj)
        } finally {
            props.history.push('/')
        }
    };


    return (
        <div className="form">
            <form className="Submit" onSubmit={quoteHandler}>
                <h5>Category:</h5>
                <select name="category" onChange={quoteDataChanged}>
                    <option />
                    {CATEGORY.map(cat => (
                        <option name='star-wars' value={cat.id}>{cat.title}</option>
                    ))}
                    </select>
                <h5>Author:</h5>
                <input
                    type="text"
                    placeholder="Author"
                    name="author"
                    value={submit.author}
                    onChange={quoteDataChanged}
                    required
                />
                <h5>Quote text:</h5>
                <textarea name="quote"
                          cols="30" rows="10"
                          placeholder="Quote"
                          value={submit.quote}
                          onChange={quoteDataChanged}
                          required
                />
                <button>Save</button>
            </form>
        </div>
    );
};

export default Submit;